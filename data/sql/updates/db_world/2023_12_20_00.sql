UPDATE `creature_template` SET `minlevel` = 72 WHERE `entry` = 23282;
UPDATE `creature` SET `position_x` = 2011.64, `position_y` = 7213.34, `position_z` = 497.217, `orientation` = 0.0361174 WHERE `guid` = 3009669;
UPDATE `smart_scripts` SET `target_x` = 2011.64, `target_y` = 7213.34 WHERE `source_type` = 1 AND `entryorguid` = 185932 AND `id` = 0;

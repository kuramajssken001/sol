UPDATE `creature` SET `wander_distance` = 0, `MovementType` = 0 WHERE `guid` IN (62126,62129,62136,62137,62142,62145,62146,62148,62150,62153,62155,62156,62159,62160,62162,62163,62165,62166,62169,62170,62172);

UPDATE `creature` SET `position_x` = -1505.05, `position_y` = -11992.4, `position_z` = 16.0258 WHERE `guid` = 62127;
UPDATE `creature` SET `position_x` = -1620.66, `position_y` = -11867, `position_z` = 10.91 WHERE `guid` = 62131;
UPDATE `creature` SET `position_x` = -1505.11, `position_y` = -11787.5, `position_z` = 21.9612 WHERE `guid` = 62134;
UPDATE `creature` SET `position_x` = -1407.04, `position_y` = -12027.3, `position_z` = 8.74414 WHERE `guid` = 62139;
UPDATE `creature` SET `position_x` = -1361.59, `position_y` = -11965.2, `position_z` = 18.4913 WHERE `guid` = 62152;
UPDATE `creature` SET `position_x` = -1667.46, `position_y` = -11891.8, `position_z` = 10.0437 WHERE `guid` = 62161;

UPDATE `creature_model_info` SET `BoundingRadius` = 4.5, `CombatReach` = 7.875 WHERE `DisplayID` = 17886;
UPDATE `creature_model_info` SET `BoundingRadius` = 4.2, `CombatReach` = 12 WHERE `DisplayID` = 20939;
UPDATE `creature_model_info` SET `BoundingRadius` = 5, `CombatReach` = 7.5 WHERE `DisplayID` = 21069;
